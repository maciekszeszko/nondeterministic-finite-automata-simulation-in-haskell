import Auto
import Control.Monad.Error
import Data.List
import Data.Maybe
import System.Environment
import System.IO
import Text.Read



---------------------
-- Inner constants --
---------------------
minLines = 4
alphabet = [(toEnum 65 :: Char) .. (toEnum 90 :: Char)]



-----------------------------
-- Inner error definitions --
-----------------------------
errorPrefix                               = "BAD INPUT"
linePrefix                                = " Line: "
callerElemPrefix                          = ", For: "
minLinesErrorMessage                      = errorPrefix ++ " File should contain at least 5 non-empty lines to set the problem correctly."
minSectionsPerTransitionLineErrorMessage  = "Transition line should contain at least 3 non-empty sections to give valid description."
statesCardinalityErrorMessage             = "States cardinality is not a valid integer."
listClosureSyntaxErrorMessage             = "Invalid list closure syntax."
duplicatedCommaErrorMessage               = "Duplicated comma."
notAnIntegerErrorMessage                  = "Not an integer."
negativeIntegerErrorMessage               = "Negative value."
startStateNotAnIntegerErrorMessage        = "Starting state is not an integer."
invalidTransitionSymbolErrorMessage       = "Invalid transition symbol"
invalidPatternSymbolErrorMessage          = "Invalid symbol in pattern."
destinationStateNotAnIntegerErrorMessage  = "Destination state is not an integer."
tooManyStatesDefinedErrorMessage          = errorPrefix ++ " There are more states than declared."
tooLessStatesDefinedErrorMessage          = errorPrefix ++ " There are less states than declared."



----------------------------
-- Inner type definitions --
----------------------------
type RawSchemaType         = (String, String, String, [String], [String])
type ErrorMessageType      = String
type ParsedTransitionType  = (Integer, String, [Integer])
type ParsedSchemaType      = (Integer, [Integer], [Integer], [ParsedTransitionType], String)
type ValidTransitionType   = (Integer, Char, [Integer])
type AutomataType          = ([Integer], [Integer], [Integer], [ValidTransitionType])
type AutomataProblemType   = (AutomataType, String)



----------
-- MAIN --
----------
main = do args <- getArgs
          case args of
            [f] -> parseFile f >>=
                     parseRawSchema >>=
                       parseData >>=
                         performQueryAndPrintResult
            otherwise -> printUsage



--------------------
-- Common parsers --
--------------------
parseInt :: Int -> ErrorMessageType -> String -> IO (Maybe Integer)
parseInt line errorMessage str =
  case readMaybe str of
    Just integer -> if integer < 0 
                       then do parserErrorHandler line negativeIntegerErrorMessage integer
                               return Nothing
                       else return $ Just integer
    Nothing  -> do parserErrorHandler line errorMessage str
                   return Nothing


parseIntegerList' :: [String] -> [Integer] -> Int -> ErrorMessageType -> IO (Maybe [Integer])
parseIntegerList' [] acc _ _                     = return $ Just acc
parseIntegerList' (x : xs) acc line errorMessage =
  case readMaybe x of
    Just integer -> if integer < 0
                       then do parserErrorHandler line negativeIntegerErrorMessage integer
                               return Nothing
                       else parseIntegerList' xs ((++) acc [integer]) line errorMessage
    Nothing      -> do parserErrorHandler line errorMessage x
                       return Nothing



-------------------------
-- Perform query logic --
-------------------------
performQueryAndPrintResult :: Maybe AutomataProblemType -> IO ()
performQueryAndPrintResult record =
  case record of
    Just ((states, initS, acceptingS, transS), pattern) -> do
      let automata = fromLists states initS acceptingS transS
      putStrLn $ show (accepts automata pattern)
    Nothing -> return ()



----------------------
-- Parse data logic --
----------------------
flattenTransitionStates :: [ParsedTransitionType] -> [Integer]
flattenTransitionStates trans =
  nub $ foldr (\(state, _, availableStates) -> (++) $ (++) [state] availableStates) [] trans


verifyStatesCardinality :: Integer -> [Integer] -> [Integer] -> [ParsedTransitionType] -> IO (Maybe [Integer])
verifyStatesCardinality card initS acceptS trans = do
  let transS = flattenTransitionStates trans
  let uniqueStatesList = nub $ foldr (\x -> (++) x) [] [initS, acceptS, transS]
  let uqslLength = toInteger $ length uniqueStatesList
  if uqslLength == card
    then return $ Just uniqueStatesList
    else if uqslLength > card 
      then do putStrLn tooManyStatesDefinedErrorMessage
              return Nothing
      else do putStrLn tooLessStatesDefinedErrorMessage
              return Nothing


buildValidTransitionSet :: [ParsedTransitionType] -> Maybe a -> IO (Maybe [ValidTransitionType])
buildValidTransitionSet trans previousComputation =
  case previousComputation of
    Just _  -> return $ Just $ foldr (\(state, symbols, availableStates) ->
                                       (++) [(state, symbol, availableStates) |
                                              symbol <- symbols]) [] trans
    Nothing -> return Nothing


parseData :: (Maybe ParsedSchemaType) -> IO (Maybe AutomataProblemType)
parseData record =
  case record of
    Just (card, initS, acceptS, trans, pattern) -> 
      verifyStatesCardinality card initS acceptS trans >>= 
        (\states -> buildValidTransitionSet trans states >>=
          (\validTrans -> if isJust validTrans
                            then return $ Just ((fromJust states,
                                                 initS,
                                                 acceptS,
                                                 fromJust validTrans), pattern)
                            else return Nothing))
    Nothing -> return Nothing



-----------------------------
-- Parse state lines logic --
-----------------------------

-- Helper functions --
wordsWhen :: (Char -> Bool) -> String -> [String]
wordsWhen predicate str =
  case dropWhile predicate str of
     ""   -> []
     str' -> w : wordsWhen predicate str'' where
       (w, str'') = break predicate str'


parseDuplicatedComma :: String -> Bool
parseDuplicatedComma str = parse' 0 str where
  parse' 2 _            = True
  parse' _ []           = False
  parse' count (x : xs) = let nCount = if x == ',' then count + 1 else 0
                          in parse' nCount xs

cutExtremes :: [a] -> [a]
cutExtremes list = drop 1 $ take ((-) (length list) 1) list


-- Monadic functions --
parseListClosureSyntax :: Int -> String -> IO (Maybe (String, Int))
parseListClosureSyntax line str =
  do let (fst, lst) = (head str, last str)
     let errorMessage = listClosureSyntaxErrorMessage
     if (/=) fst '['
       then do parserErrorHandler line errorMessage fst
               return Nothing
       else if (/=) lst ']'
         then do parserErrorHandler line errorMessage lst
                 return Nothing
         else return $ Just (cutExtremes str, line)


parseListCommaSyntax :: Maybe (String, Int) -> IO (Maybe ([String], Int))
parseListCommaSyntax record =
  case record of
    Just (str, line) -> do if parseDuplicatedComma str
                             then do (parserErrorHandler line duplicatedCommaErrorMessage str)
                                     return Nothing
                             else return $ Just $ (wordsWhen (== ',') str, line)
    Nothing -> return Nothing


parseIntegerList :: Maybe ([String], Int) -> IO (Maybe [Integer])
parseIntegerList record =
  case record of
    Just (strList, line) -> parseIntegerList' strList [] line notAnIntegerErrorMessage
    Nothing              -> return Nothing


parseStatesList :: Int -> String -> Maybe a -> IO (Maybe [Integer])
parseStatesList line statesString previousComputation =
  case previousComputation of
    Just _  -> parseListClosureSyntax line statesString >>=
                parseListCommaSyntax >>= parseIntegerList
    Nothing -> return Nothing



-----------------------------
-- Parse transitions logic --
-----------------------------
parseSymbols' :: String -> Int -> ErrorMessageType -> IO (Maybe String)
parseSymbols' [] _ _                     = return $ Just ""
parseSymbols' (x : xs) line errorMessage
  | elem x alphabet = parseSymbols' xs line errorMessage
  | otherwise       = do parserErrorHandler line errorMessage x
                         return Nothing

parseSymbols :: Int -> String -> Maybe a -> IO (Maybe String)
parseSymbols line strLine previousComputation =
  case previousComputation of
    Just _  -> do sym <- parseSymbols' strLine line invalidTransitionSymbolErrorMessage
                  case sym of
                    Just _  -> return $ Just strLine
                    Nothing -> return Nothing
    Nothing -> return Nothing


parseDestinationStates :: Int -> [String] -> Maybe a -> IO (Maybe [Integer])
parseDestinationStates line strList previousComputation =
  case previousComputation of
    Just _  -> parseIntegerList' strList [] line destinationStateNotAnIntegerErrorMessage
    Nothing -> return Nothing
  

parseTransitionLine :: Int -> String -> IO (Maybe ParsedTransitionType)
parseTransitionLine line strLine =
  let dividedStrLine = words strLine
  in if length dividedStrLine < 3
       then do parserErrorHandler line minSectionsPerTransitionLineErrorMessage strLine
               return Nothing
       else do let startState = head dividedStrLine
               let symbols    = head $ tail dividedStrLine
               let destinationStates = tail $ tail dividedStrLine
               parseInt line startStateNotAnIntegerErrorMessage startState >>=
                 (\startS -> parseSymbols line symbols startS >>=
                   (\symb -> parseDestinationStates line destinationStates symb >>=
                     (\destinationS -> if isJust destinationS
                                         then return $ Just (fromJust startS,
                                                             fromJust symb,
                                                             fromJust destinationS)
                                         else return Nothing)))

 
parseTransitions :: Int -> [String] -> Maybe a -> IO (Maybe ([ParsedTransitionType], Int))
parseTransitions line transitions previousComputation =
  case previousComputation of
    Just _  -> parseTransitions' line transitions []
                 where
                   parseTransitions' :: Int -> [String] -> [ParsedTransitionType] -> IO (Maybe ([ParsedTransitionType], Int))
                   parseTransitions' l [] acc       = return $ Just (acc, l)
                   parseTransitions' l (x : xs) acc = do
                     trans <- parseTransitionLine line x
                     case trans of
                       Just record -> parseTransitions' ((+) l 1) xs ((++) acc [record])
                       Nothing     -> return Nothing
    Nothing -> return Nothing


-------------------------
-- Parse pattern logic --
-------------------------
parsePattern :: String -> Maybe ([ParsedTransitionType], Int) -> IO (Maybe String)
parsePattern pattern previousComputation =
  case previousComputation of
    Just (trans, line) -> do parsed <- parseSymbols' pattern line invalidPatternSymbolErrorMessage 
                             case parsed of
                               Just _   -> return $ Just pattern
                               Nothing  -> return Nothing
    Nothing            -> return Nothing



-----------------------------------
-- Parse raw data abstract logic --
-- --------------------------------
parseRawSchema :: (Maybe RawSchemaType) -> IO (Maybe ParsedSchemaType)
parseRawSchema rawSchema =
  case rawSchema of
    Just (statesCard, initStates, acceptingStates, transitions, pattern) -> do
      parseInt 1 statesCardinalityErrorMessage statesCard >>=
        (\cardinality -> parseStatesList 2 initStates cardinality >>=
          (\iStates -> parseStatesList 3 acceptingStates iStates >>=
            (\aStates -> parseTransitions 4 transitions aStates >>=
              (\record -> parsePattern (head pattern) record >>=
                (\pattern -> if isJust pattern
                               then return $ Just (fromJust cardinality,
                                                   fromJust iStates,
                                                   fromJust aStates,
                                                   (\(trans, _) -> trans) (fromJust record),
                                                   fromJust pattern)
                               else return Nothing )))))
    Nothing -> return Nothing



----------------------
-- Parse file logic --
----------------------
parseFile :: FilePath -> IO (Maybe RawSchemaType)
parseFile file = maybeReadFile file >>= parseContents


maybeReadFile :: String -> IO (Maybe String)
maybeReadFile fileName =
  (do contents <- readFile fileName;
       return $ Just contents)
     `catchError`
  (readFileErrorHandler fileName)


parseContents :: Maybe String -> IO (Maybe RawSchemaType)
parseContents fileContents = do
  case fileContents of
    Just contents -> do let fileLines = filter (not . null) (lines contents)
                        if (length fileLines) > minLines
                          then return $ Just $ buildRawSchema fileLines
                          else do putStrLn minLinesErrorMessage
                                  return Nothing
    Nothing         -> return Nothing


buildRawSchema :: [String] -> RawSchemaType
buildRawSchema (statesCardinality : initStates : acceptingStates : rest) =
  let
    (transitions, patterns) = splitAt ((length rest) -1)  rest
  in (statesCardinality, initStates, acceptingStates, transitions, patterns)


--------------
-- Handlers --
--------------
readFileErrorHandler :: String -> IOError -> IO (Maybe String)
readFileErrorHandler fileName err = do putStrLn (show err)
                                       return Nothing

parserErrorHandler :: (Show a) => Int -> String -> a -> IO ()
parserErrorHandler line errorMessage elem =
  do putStrLn $ errorPrefix ++ linePrefix ++ (show line) ++ callerElemPrefix
               ++ (show elem) ++ " " ++ errorMessage

                              
----------------
-- Usage info --
----------------
printUsage :: IO ()
printUsage = do pname <- getProgName
                putStrLn $ "Usage: " ++ pname ++ " [file]"               

