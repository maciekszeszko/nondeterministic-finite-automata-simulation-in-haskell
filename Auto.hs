module Auto( Auto
           , accepts
           , emptyA, epsA, symA
           , leftA, rightA, sumA, thenA
           , fromLists, toLists, getAcceptingStateList
           ) where

import Data.List



------------------
-- Public types --
------------------
data Auto a q               = A { states      :: [q]
                                , initStates  :: [q]
                                , isAccepting :: q -> Bool
                                , transition  :: q -> a -> [q]
                                }



------------------------
-- Inner automata API --
------------------------
generateEnumValues :: (Enum a, Bounded a) => Auto a q -> [a]
generateEnumValues aut = [minBound ..]


getUniqueSuccessors :: Eq q => a -> Auto a q -> [q] -> [q]
getUniqueSuccessors label automata currentStatesSet =
  nub $ concat $ map (getSuccessors) currentStatesSet where
    getSuccessors state =
      [successors | successors <- (transition automata) state label]


isAutomataLanguageEmpty :: Auto a q -> Bool
isAutomataLanguageEmpty automata = 
  (
    (||) 
      (not
        (foldr (\x -> (||) ((isAccepting automata) x)) False (states automata))
      )
      (null (initStates automata))
  )


notEmptyList :: [a] -> Bool
notEmptyList list = not (null list)


getAcceptingStateList :: (q -> Bool) -> [q] -> [q]
getAcceptingStateList isAcceptingF stateList = filter isAcceptingF stateList



-------------------------
-- Public automata API --
-------------------------
accepts :: Eq q => Auto a q -> [a] -> Bool
accepts automata pattern = accepts' pattern (initStates automata) where
  accepts' [] statesA = notEmptyList (intersect statesA
                                        (getAcceptingStateList
                                           (isAccepting automata)
                                           (states automata)))
  accepts' (label : patternSuffix) [] = False
  accepts' (label : patternSuffix) statesA =
    let
       acceptingCondition successors     =
         (&&) (any (isAccepting automata) successors) (null patternSuffix)
       proceedPatternProcessing successors 
         | acceptingCondition successors = True
         | otherwise                     = accepts' patternSuffix successors
    in proceedPatternProcessing (getUniqueSuccessors label automata statesA)


emptyA :: Auto a ()
emptyA = A [()] [()] (\state -> False) (\state label -> [])


epsA :: Auto a ()
epsA = A [()] [()] (\state -> True) (\state label -> [])


symA :: Eq a => a -> Auto a Bool
symA label = fromLists [False, True] [False] [True] [(False, label, [True])]


leftA :: Auto a q -> Auto a (Either q r) 
leftA automata =
  let
    statesE      = map Left (states automata)
    initStatesE  = map Left (initStates automata)
    isAcceptingE = (\(Left state) -> (isAccepting automata) state)
    transitionE  = (\(Left state) label -> map Left
                     ((transition automata) state label))
  in A statesE initStatesE isAcceptingE transitionE

        
rightA :: Auto a q -> Auto a (Either p q) 
rightA automata =
  let
    statesE      = map Right (states automata)
    initStatesE  = map Right (initStates automata)
    isAcceptingE = (\(Right state) -> (isAccepting automata) state)
    transitionE  = (\(Right state) label -> map Right
                     ((transition automata) state label))
  in A statesE initStatesE isAcceptingE transitionE

          
fromLists :: (Eq q, Eq a) => [q] -> [q] -> [q] -> [(q,a,[q])] -> Auto a q
fromLists states initStates acceptingStates transitionList =
  let
    isAccepting = (\state -> elem state acceptingStates)
    transition = (\s l -> concat [successors | 
                            (state, label, successors) <- transitionList,
                             state == s,
                             label == l])
  in (A states initStates isAccepting transition)


toLists :: (Enum a, Bounded a) => Auto a q -> ([q],[q],[q],[(q,a,[q])])
toLists automata =
  let
    enumList = generateEnumValues automata
    acceptingStates = getAcceptingStateList (isAccepting automata)
                                            (states automata)
    getTransitions state =
      foldr (\label -> (++) [(state, label, successors) |
                     successors <- [(transition automata) state label],
                     notEmptyList successors])
      []
      enumList
    transitionList = concat $ map getTransitions (states automata)
  in (states automata, initStates automata, acceptingStates, transitionList)


thenA :: Auto a q1 -> Auto a q2 -> Auto a (Either q1 q2)
thenA automata1 automata2 =
  if (||)
       (isAutomataLanguageEmpty automata2)
       (isAutomataLanguageEmpty automata1)
    then let automata' = leftA automata1
         in A (states automata') (initStates automata')
            (\(Left x) -> False) (transition automata')
    else
      let
        leftAutomata1                   = leftA automata1
        rightAutomata2                  = rightA automata2
        statesC                         = (++) (states leftAutomata1)
                                               (states rightAutomata2)
        initStatesC                     = (++) (initStates leftAutomata1)
                                               (if notEmptyList
                                                  (getAcceptingStateList
                                                    (isAccepting leftAutomata1)
                                                    (initStates leftAutomata1)
                                                  )
                                                  then
                                                    (initStates rightAutomata2)
                                                  else []
                                               )
        isAcceptingC (Left state)       = False
        isAcceptingC (Right state)      = (isAccepting rightAutomata2)
                                            (Right state)
        transitionC (Right state) label =
          map Right ((transition automata2) state label)
        transitionC (Left state) label = (++)
          (map Left ((transition automata1) state label))
          (
            if notEmptyList (getAcceptingStateList
                               (isAccepting automata1)
                               ((transition automata1) state label))
                 then (map Right (initStates automata2))
                 else []
          )
       in A statesC initStatesC isAcceptingC transitionC


sumA :: Auto a q1 -> Auto a q2 -> Auto a (Either q1 q2)
sumA automata1 automata2 =
  if isAutomataLanguageEmpty automata2
    then leftA automata1
    else if isAutomataLanguageEmpty automata1
      then rightA automata2
      else
        let
          leftAutomata1                   = leftA automata1
          rightAutomata2                  = rightA automata2
          statesS                         = (++) (states leftAutomata1)
                                                 (states rightAutomata2)
          initStatesS                     = (++) (initStates leftAutomata1)
                                                 (initStates rightAutomata2)
          isAcceptingS (Left state)       = (isAccepting leftAutomata1)
                                              (Left state)
          isAcceptingS (Right state)      = (isAccepting rightAutomata2)
                                              (Right state)
          transitionS (Left state) label  =
            map Left ((transition automata1) state label)
          transitionS (Right state) label =
            map Right ((transition automata2) state label)
        in A statesS initStatesS isAcceptingS transitionS



----------------------------------
-- Show automata instance tools --
----------------------------------
getStates (states, _, _, _)                           = states
getInitStates (_, initStates, _, _)                   = initStates
getAcceptingStatesList (_, _, acceptingStatesList, _) = acceptingStatesList
getTransitionList (_, _, _, transitionList)           = transitionList

getRepresentation repr = 
     "Automata representation\n" ++ "\tstates: " ++ show (getStates repr)
     ++ "\n\tinit states: " ++ show (getInitStates repr)
     ++ "\n\taccepting states: " ++ show (getAcceptingStatesList repr)
     ++ "\n\ttransition list: " ++ show (getTransitionList repr)

instance (Show a, Enum a, Bounded a, Show q) => Show (Auto a q) where
  show automata = getRepresentation (toLists automata)

